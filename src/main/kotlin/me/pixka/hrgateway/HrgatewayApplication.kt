package me.pixka.hrgateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HrgatewayApplication

fun main(args: Array<String>) {
	runApplication<HrgatewayApplication>(*args)
}
